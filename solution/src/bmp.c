#include "bmp.h"
#include "image.h"
#include "iostates.h"
#include "work_file.h"
#include <stdbool.h>

#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BFTYPE 0x4D42

static struct bmp_header create_header(const uint32_t width, const uint32_t height, const uint32_t padding){
    const uint32_t img_size = height*padding + width*height*sizeof(struct pixel);
    return (struct bmp_header) {
            .bfType = BFTYPE,
            .bfileSize = (sizeof( struct bmp_header )) + img_size,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BISIZE,
            .biWidth = (uint32_t) width,
            .biHeight = (uint32_t) height,
            .biPlanes = BIPLANES,
            .biBitCount = BIBITCOUNT,
            .biSizeImage = img_size,
    };
}

static enum bmp_read_status check_header (const struct bmp_header * header) {
    if (header->bfType != 0x4d42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    if (header->biSize != 40 || header->biCompression != 0 || (header->bfileSize != header->bOffBits + header->biSizeImage)) {
        return READ_INVALID_HEADER;
    }

    return READ_OK;
}

uint32_t get_padding (uint32_t width) {
    return (4 - (sizeof(struct pixel) * width) % 4);
}

static enum pixels_read_status read_pixels(FILE* file, struct image* img) {
    struct pixel* pixels = img->data;
    struct pixel* ptr = pixels;
    const uint32_t padding = get_padding(img->width);

    for(uint32_t i = 0; i < img->height; i++) {
        const size_t px_count = fread(ptr, sizeof(struct pixel), img->width, file);
        const int fseek_success = fseek(file, padding, SEEK_CUR);
        if (px_count != img->width || fseek_success != 0) {
            free(pixels);
            return READ_PIXELS_ERROR;
        }
        ptr = ptr + img->width;
    }
    return READ_PIXELS_OK;
}

static enum bmp_write_status write_pixels(FILE* file, struct pixel* pixels, const uint32_t width, const uint32_t height, const uint32_t padding_bytes) {
    struct pixel* ptr = pixels;
    for(int i = 0; i < height; i++){
        const size_t cnt = fwrite(ptr, sizeof(struct pixel), width, file);
        const size_t pad = fwrite((void*) ptr, 1, padding_bytes, file);
        if (cnt != width || pad != padding_bytes) {

            return WRITE_ERROR;
        }
        ptr = ptr + width;
    }
    return WRITE_OK;
}

enum bmp_read_status from_bmp(FILE* in, struct image* image) {
	struct bmp_header header = { 0 };
	if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
		return READ_INVALID_HEADER;
	}
	if (check_header(&header) != READ_OK) {
		return check_header(&header);
	}
	*image = create_image(header.biWidth, header.biHeight);
	const enum pixels_read_status px_read = read_pixels(in, image);
	if (px_read == READ_PIXELS_ERROR) {
		return READ_INVALID_BITS;
	}
	return READ_OK;
}

enum bmp_write_status to_bmp(FILE* out, struct image const* image) {
	const uint32_t width = image->width;
	const uint32_t height = image->height;
	const uint32_t padding = get_padding(width);
	struct pixel* px_array = image->data;
	struct bmp_header header = create_header(width, height, padding);

	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
		return WRITE_ERROR;
	}
	return write_pixels(out, px_array, width, height, padding);
}


void read_bmp_message(enum bmp_read_status rs) {
	fprintf(stderr, "%s", read_bmp_array[rs]);
}

void write_bmp_message(enum bmp_write_status ws) {
    fprintf(stderr, "%s", write_bmp_array[ws]);
}

struct image read_image(char* in_path) {
	FILE* in = { 0 };

	enum read_file_status read_file_st = read_file(&in, in_path);
	read_file_message(read_file_st);
	if (read_file_st != 0) return (struct image) { 0 };
	
	struct image source;

	enum bmp_read_status bmp_read_st = from_bmp(in, &source);
	read_bmp_message(bmp_read_st);
	if (bmp_read_st != 0) return (struct image) { 0 };

	close_file_message(close_file(in));

	return source;
}

bool write_image(char* out_path, struct image img) {
	FILE* out = { 0 };

	enum write_file_status write_file_st = write_file(&out, out_path);
	write_file_message(write_file_st);
	if (write_file_st != 0) return false;

	enum bmp_write_status bmp_write_st = to_bmp(out, &img);
	write_bmp_message(bmp_write_st);
	if (bmp_write_st != 0) return false;

	enum close_file_status close_file_st = close_file(out);
	close_file_message(close_file_st);
	if (close_file_st != 0) return false;
	
	return true;
}

