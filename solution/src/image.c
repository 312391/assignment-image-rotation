#include "image.h"
#include <malloc.h>

struct pixel get_pixel(struct image const* image, size_t index1, size_t index2) {
    return image->data[(image->height - 1 - index2) * image->width + index1];
}

void set_pixel(struct image* image, struct pixel pixel, size_t index1, size_t index2) {
    image->data[index1 * image->width + index2] = pixel;
}

struct image create_image(const uint64_t width, const uint64_t height){
    struct pixel* pixels = malloc(sizeof(struct pixel) * (width * height));
    return (struct image) { .width = width, .height = height, .data = pixels };
}

void destroy_image(struct image* image) {
    free(image->data);
}

