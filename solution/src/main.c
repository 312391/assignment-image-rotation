#include "rotate.h"
#include "bmp.h"
#include "image.h"
#include <stdio.h>



int main( int argc, char** argv ) {

    (void) argc; (void) argv;
    if (argc != 3) {
        printf("Correct format : image-transform `dst` `src`");
        return -1;
    }

    struct image source = read_image(argv[1]);

    struct image result = image_transform(&source, rotate);

    if (!write_image(argv[2], result)) return 1;

    destroy_image(&source);

    destroy_image(&result);

    return 0;
}


