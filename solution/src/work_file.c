#include "work_file.h"
#include "iostates.h"
#include <stdio.h>


enum read_file_status read_file (FILE** file, const char * path) {
    *file = fopen(path, "rb");
    if (file != NULL) {
        return FILE_READ_OK;
    } else {
        return FILE_READ_ERROR;
    }
}

void read_file_message(enum read_file_status rs) {
    fprintf(stderr, "%s", file_read_status_array[rs]);
}

enum write_file_status write_file (FILE** file, const char * path) {
    *file = fopen(path, "wb");
    if (file != NULL) {
        return FILE_WRITE_OK;
    } else {
        return FILE_WRITE_ERROR;
    }
}

void write_file_message(enum write_file_status ws) {
    fprintf(stderr, "%s", file_write_status_array[ws]);
}

enum close_file_status close_file (FILE* file) {
    if (file == NULL) {
        return FILE_CLOSE_ERROR;
    }
    fclose(file);
    return FILE_CLOSE_OK;
}

void close_file_message(enum close_file_status cs) {
    fprintf(stderr, "%s", file_close_status_array[cs]);
}



