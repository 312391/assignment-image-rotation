#include "rotate.h"
#include "image.h"
#include <malloc.h>
#include <stdio.h>


struct image rotate(struct image const* image) {
    size_t height = image->width;
    size_t width = image->height;

    struct image result = {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * image->width * image->height)
    };
    for (size_t i = 0; i < image->width; i++) {
        for (size_t j = 0; j < image->height; j++) {
            set_pixel(&result, get_pixel(image, i, j), i, j);
        }
    }
    return result;
}



struct image image_transform(struct image const* image, struct image (f)(struct image const*)) {
    return f(image);
}

