#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"

struct image rotate(struct image const* image);
struct image image_transform(struct image const* image, struct image (f)(struct image const*));

#endif
