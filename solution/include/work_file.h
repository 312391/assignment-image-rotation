#ifndef WORK_FILE
#define WORK_FILE

#include "iostates.h"
#include <stdint.h>
#include <malloc.h>
#include "stdio.h"

enum read_file_status  {
    FILE_READ_OK = 0,
    FILE_READ_ERROR
};

static char* const file_read_status_array[] = {
        [FILE_READ_OK] = "file successfully open to read\n",
        [FILE_READ_ERROR] = "reading error\n"
};

enum read_file_status read_file (FILE** file, const char * path);
void read_file_message(enum read_file_status rs);

enum  write_file_status {
    FILE_WRITE_OK = 0,
    FILE_WRITE_ERROR
};

static char* const file_write_status_array[] = {
        [FILE_WRITE_OK] = "file successfully open to write\n",
        [FILE_WRITE_ERROR] = "writing error\n"
};

enum write_file_status write_file (FILE** file, const char * path);
void write_file_message(enum write_file_status ws);

enum close_file_status {
    FILE_CLOSE_OK = 0,
    FILE_CLOSE_ERROR
};

static char* const file_close_status_array[] = {
        [FILE_CLOSE_OK] = "file successfully closed\n",
        [FILE_CLOSE_ERROR] = "file already closed\n"
};

enum close_file_status close_file (FILE* file);
void close_file_message(enum close_file_status cs);

#endif
