#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <malloc.h>
#include <stdio.h>

struct __attribute__((packed)) pixel
{
  uint8_t b, g, r;
};

struct image
{
  uint64_t width, height;
  struct pixel *data;
};


struct pixel get_pixel(struct image const* image, size_t index1, size_t index2);

void set_pixel(struct image* image, struct pixel pixel, size_t index1, size_t index2);

struct image create_image(uint64_t width, uint64_t height);

void destroy_image(struct image* image);

#endif

