#ifndef BMP_H
#define BMP_H

#include "image.h"
#include "iostates.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static char* const read_bmp_array[] = {
		[READ_OK] = "bmp successfully read\n",
		[READ_INVALID_SIGNATURE] = "wrong format\n",
		[READ_INVALID_BITS] = "invalid count of bits\n",
		[READ_INVALID_HEADER] = "invalid header arguments\n",
};


static char* const write_bmp_array[] = {
		[WRITE_OK] = "bmp successfully wrote\n",
		[WRITE_ERROR] = "writing error\n"
};

struct image read_image(char* in_path);
bool write_image(char* out_path, struct image img);

#endif
