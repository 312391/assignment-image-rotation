#ifndef IOSTATES_H
#define IOSTATES_H

#include <stdio.h>
#include <stdint.h>
#include "image.h"

enum bmp_read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum bmp_write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum pixels_read_status {
    READ_PIXELS_OK = 0,
    READ_PIXELS_ERROR
};

enum bmp_read_status  from_bmp(FILE* in, struct image* image);
void read_bmp_message(enum bmp_read_status rs);

enum bmp_write_status to_bmp( FILE* out, struct image const* image );
void write_bmp_message(enum bmp_write_status ws);


#endif


